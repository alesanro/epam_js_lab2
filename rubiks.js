var RubikCubeLib = (function () {

	var FACE_FRONT = 'front',
		FACE_BACK = 'back',
		FACE_RIGHT = 'right',
		FACE_LEFT = 'left',
		FACE_UP = 'up',
		FACE_DOWN = 'down';

	var pairs = {
		'front': FACE_BACK,
		'back': FACE_FRONT,
		'right': FACE_LEFT,
		'left': FACE_RIGHT,
		'up': FACE_DOWN,
		'down': FACE_UP
	};

	var facesList = [FACE_FRONT, FACE_BACK, FACE_UP, FACE_DOWN, FACE_LEFT, FACE_RIGHT];

	function isOppositeSides(side1, side2) {
		return pairs[side1] === side2;
	}

	var ItemNotFound = -1;
					
	/*-------------------------------------------------------------------------------------------
											Tile	
	-------------------------------------------------------------------------------------------*/

	function Tile(color) {
		this.color = color;
	}

	function TileFromTile(tile) {
		return new Tile(tile.color);
	}

	/*-------------------------------------------------------------------------------------------
											Row
	-------------------------------------------------------------------------------------------*/

	function Row() {
		this.leftTile = arguments[0];
		this.centerTile = arguments[1];
		this.rightTile = arguments[2];
	}

	function RowFromRow(row) {
		return new Row(TileFromTile(row.leftTile), TileFromTile(row.centerTile), TileFromTile(row.rightTile));
	}

	Row.prototype.hasSingleColor = function() {
		return this.leftTile.color === this.centerTile.color && this.leftTile.color === this.rightTile.color;
	};

	/*-------------------------------------------------------------------------------------------
											Face
	-------------------------------------------------------------------------------------------*/

	function Face(name, rows) {
		this.topRow = rows[0];
		this.centerRow = rows[1];
		this.bottomRow = rows[2];
		this.name = name;
		this.toNormalState = null;
	}

	Face.prototype.leftColumn = function() {
		return [this.topRow.leftTile, this.centerRow.leftTile, this.bottomRow.leftTile];
	};
	Face.prototype.setLeftColumn = function(column) {
		this.topRow.leftTile = column[0];
		this.centerRow.leftTile = column[1];
		this.bottomRow.leftTile = column[2];
	};
	Face.prototype.centerColumn = function() {
		return [this.topRow.centerTile, this.centerRow.centerTile, this.bottomRow.centerTile];
	};
	Face.prototype.setCenterColumn = function(column) {
		this.topRow.centerTile = column[0];
		this.centerRow.centerTile = column[1];
		this.bottomRow.centerTile = column[2];
	};
	Face.prototype.rightColumn = function() {
		return [this.topRow.rightTile, this.centerRow.rightTile, this.bottomRow.rightTile];
	};
	Face.prototype.setRightColumn = function(column) {
		this.topRow.rightTile = column[0];
		this.centerRow.rightTile = column[1];
		this.bottomRow.rightTile = column[2];
	};

	Face.prototype.rotate = function(clockwise) {

		if (clockwise) {
			var topRow = [this.topRow.leftTile, this.topRow.centerTile, this.topRow.rightTile],
				bottomRow = [this.bottomRow.leftTile, this.bottomRow.centerTile, this.bottomRow.rightTile],
				leftColumn = new Row(this.leftColumn()[2],this.leftColumn()[1],this.leftColumn()[0]),
				rightColumn = new Row(this.rightColumn()[2],this.rightColumn()[1],this.rightColumn()[0]);

			this.topRow = leftColumn;
			this.bottomRow = rightColumn;
			this.setLeftColumn(bottomRow);
			this.setRightColumn(topRow);
		} else {
			var topRow = [this.topRow.rightTile, this.topRow.centerTile, this.topRow.leftTile],
				bottomRow = [this.bottomRow.rightTile, this.bottomRow.centerTile, this.bottomRow.leftTile],
				leftColumn = new Row(this.leftColumn()[0],this.leftColumn()[1],this.leftColumn()[2]),
				rightColumn = new Row(this.rightColumn()[0],this.rightColumn()[1],this.rightColumn()[2]);
			
			this.topRow = rightColumn;
			this.bottomRow = leftColumn;
			this.setRightColumn(bottomRow);
			this.setLeftColumn(topRow);
		}
	};

	Face.prototype.reflectX = function() {
		var topRow = RowFromRow(this.topRow);
		this.topRow = this.bottomRow;
		this.bottomRow = topRow; 
	}

	Face.prototype.reflectY  = function() {
		var rightColumn = this.rightColumn();
		this.setRightColumn(this.leftColumn());
		this.setLeftColumn(rightColumn);	
	}

	Face.prototype.hasSingleColor = function() {
		return this.topRow.hasSingleColor() && this.centerRow.hasSingleColor() && this.bottomRow.hasSingleColor() &&
			this.topRow.leftTile.color === this.centerRow.leftTile.color && this.topRow.leftTile.color === this.bottomRow.leftTile.color;
	};


	/*-------------------------------------------------------------------------------------------
											Cube
	-------------------------------------------------------------------------------------------*/

	function Cube() {
		this.faces = {};

		var facesWithColors = {
			'front': 'red',
			'back': 'orange',
			'right': 'white',
			'left': 'yellow	',
			'up': 'green',
			'down': 'blue'
		};

		for(var faceKey in facesWithColors) {
			var color = facesWithColors[faceKey];

			var rows = [];
			for (var i = 2; i >= 0; i--) {
				rows.push(new Row(new Tile(color), new Tile(color), new Tile(color)));
			}

			var face = new Face(faceKey, rows);
			this.faces[faceKey] = face;
		}
	}

	Cube.prototype.rotateCubeVertical = function(clockwise) {
		this.backToNormalState();
		var faces = verticalFaces(clockwise);
		shiftByOne.call(this, faces);
	}

	Cube.prototype.rotateCubeHorizontal = function(clockwise) {
		this.backToNormalState();
		var faces = horizontalFaces(clockwise);
		shiftByOne.call(this, faces);
	}

	Cube.prototype.rotateLeft = function(clockwise) {
		this.backToNormalState();
		this.faces[FACE_LEFT].rotate(clockwise);
		var faces = verticalFaces(clockwise);

		enumFaces.call(this, faces, function (face) {
			return face.leftColumn();
		}, function (face, value) {
			face.setLeftColumn(value);
		});
	};

	Cube.prototype.rotateCenterV = function(clockwise) {
		this.backToNormalState();
		var faces = verticalFaces(clockwise);

		enumFaces.call(this, faces, function (face) {
			return face.centerColumn();
		}, function (face, value) {
			face.setCenterColumn(value);
		});
	};

	Cube.prototype.rotateRight = function(clockwise) {
		this.backToNormalState();
		this.faces[FACE_RIGHT].rotate(clockwise);
		var faces = verticalFaces(clockwise);

		enumFaces.call(this, faces, function (face) {
			return face.rightColumn();
		}, function (face, value) {
			face.setRightColumn(value);
		});
	};

	Cube.prototype.rotateTop = function(clockwise) {
		this.backToNormalState();
		this.faces[FACE_UP].rotate(clockwise);
		var faces = horizontalFaces(clockwise);

		enumFaces.call(this, faces, function (face) {
			return face.topRow;
		}, function (face, value) {
			face.topRow = value;
		});
	};

	Cube.prototype.rotateCenterH = function(clockwise) {
		this.backToNormalState();
		var faces = horizontalFaces(clockwise);

		enumFaces.call(this, faces, function (face) {
			return face.centerRow;
		}, function (face, value) {
			face.centerRow = value;
		});
	};

	Cube.prototype.rotateBottom = function(clockwise) {
		this.backToNormalState();
		this.faces[FACE_DOWN].rotate(clockwise);
		var faces = horizontalFaces(clockwise);

		enumFaces.call(this, faces, function (face) {
			return face.bottomRow;
		}, function (face, value) {
			face.bottomRow = value;
		});
	};

	Cube.prototype.isSolved = function() {
		var solved = true;
		for(var faceKey in this.faces) {
			solved = this.faces[faceKey].hasSingleColor();
			if (!solved) {
				return false;
			}
		}

		return solved;
	};

	Cube.prototype.mapFrontFace = function() {
		var frontFace = this.faces[FACE_FRONT];
		if (frontFace.toNormalState !== null) {
			return frontFace;
		}

		rotateVirtualCube.apply(this);

		return frontFace;
	};

	Cube.prototype.backToNormalState = function() {
		if (this.faces[FACE_FRONT].toNormalState !== null && this.faces[FACE_FRONT].toNormalState !== 'undefined') {
			this.faces[FACE_FRONT].toNormalState();
			this.faces[FACE_FRONT].toNormalState = null;
		}
	}


	//====================================================================================
	
	function shiftByOne (faces) {
		var past = this.faces[faces[0]];
		for (var i = 0; i <= faces.length - 2; i++) {
			this.faces[faces[i]] = this.faces[faces[i+1]];
		};
		this.faces[faces[faces.length - 1]] = past;
	}

	function verticalFaces(clockwise) {
		var faces = [FACE_FRONT, FACE_DOWN, FACE_BACK, FACE_UP];
		if (!clockwise) {
			faces = faces.reverse();
		}

		return faces;
	}

	function horizontalFaces(clockwise) {
		var faces = [FACE_FRONT, FACE_RIGHT, FACE_BACK, FACE_LEFT];
		if (!clockwise) {
			faces = faces.reverse();
		}

		return faces;
	}


	function enumFaces(faces, getCb, setCb) {
		var past = getCb(this.faces[faces[0]]);
		for (var i = 0; i <= faces.length - 2; i++) {
			setCb(this.faces[faces[i]], getCb(this.faces[faces[i+1]]));
		};
		setCb(this.faces[faces[faces.length - 1]], past);
	}

	function rotateVirtualCube() {
		var _this = this;
		var frontFace = _this.faces[FACE_FRONT];

		if (shouldDoNothing()) {
			return;
		} else if (shouldReflectByX()) {
			frontFace.reflectX();
			frontFace.toNormalState = function() {
				frontFace.reflectX();
			};
		} else if (shouldReflectByY()) {
			frontFace.reflectY();
			frontFace.toNormalState = function() {
				frontFace.reflectY();
			};
		} else if (shouldRotateClockwise90()) {
			frontFace.rotate(true);
			frontFace.toNormalState = function() {
				frontFace.rotate(false);
			};
		} else if (shouldRotateCounterClockwise90()) {
			frontFace.rotate(false);
			frontFace.toNormalState = function() {
				frontFace.rotate(true);
			};
		} else if (shouldRotate180AndReflectByX()) {
			frontFace.rotate(true);
			frontFace.rotate(true);
			frontFace.reflectX();
			frontFace.toNormalState = function() {
				frontFace.reflectX();
				frontFace.rotate(true);
				frontFace.rotate(true);
			};
		} else if (shouldRotate180AndReflectByY()) {
			frontFace.rotate(true);
			frontFace.rotate(true);
			frontFace.reflectY();
			frontFace.toNormalState = function() {
				frontFace.reflectY();
				frontFace.rotate(true);
				frontFace.rotate(true);
			};
		} else if (shouldRotate180()) {
			frontFace.rotate(true);
			frontFace.rotate(true);
			frontFace.toNormalState = function() {
				frontFace.rotate(true);
				frontFace.rotate(true);
			};
		} else if (shouldRotateCounterClockwise90AndReflectByY()) {
			frontFace.rotate(false);
			frontFace.reflectY();
			frontFace.toNormalState = function() {
				frontFace.reflectY();
				frontFace.rotate(false);
			};
		} else if (shouldRotateClockwise90AndReflectByY()) {
			frontFace.rotate(true);
			frontFace.reflectY();
			frontFace.toNormalState = function() {
				frontFace.reflectY();
				frontFace.rotate(true);
			};
		} else {
			var state = "";
			for (var faceName in _this.faces) {
				state += faceName + ": " + _this.faces[faceName].name + ";   ";
			}
			console.warn("No such state! \n" + state);
		}
		
		function shouldDoNothing() {
			var oppositesCounter = 0;
			var equalCounter = 0;
			for (var faceName in _this.faces) {
				oppositesCounter += isOppositeSides(faceName, _this.faces[faceName].name) ? 1 : 0;
				equalCounter += _this.faces[faceName].name === faceName;
			};

			return ((_this.faces[FACE_UP].name === FACE_UP) && (_this.faces[FACE_DOWN].name === FACE_DOWN) 
				|| (_this.faces[FACE_LEFT].name === FACE_LEFT) && (_this.faces[FACE_RIGHT].name === FACE_RIGHT)
				|| equalCounter === facesList.length)
			&& (oppositesCounter === 0);
		}

		function shouldReflectByX() {
			var oppositesCounter = 0;
			for (var faceName in _this.faces) {
				oppositesCounter += isOppositeSides(faceName, _this.faces[faceName].name) ? 1 : 0;
			};

			return (_this.faces[FACE_LEFT].name === FACE_LEFT) 
			&& (_this.faces[FACE_RIGHT].name === FACE_RIGHT)
			&& (oppositesCounter === facesList.length - 2);
		}

		function shouldReflectByY() {
			var oppositesCounter = 0;
			for (var faceName in _this.faces) {
				oppositesCounter += isOppositeSides(faceName, _this.faces[faceName].name) ? 1 : 0;
			};

			return (_this.faces[FACE_UP].name === FACE_UP) 
			&& (_this.faces[FACE_DOWN].name === FACE_DOWN)
			&& (oppositesCounter === facesList.length - 2);
		}

		function shouldRotateClockwise90() {
			return and3Sides(FACE_UP, FACE_FRONT, FACE_RIGHT)
			|| and3Sides(FACE_DOWN, FACE_BACK, FACE_LEFT)
			|| and3Sides(FACE_RIGHT, FACE_DOWN, FACE_BACK)
			|| and3Sides(FACE_LEFT, FACE_DOWN, FACE_FRONT)
			|| and3Sides(FACE_FRONT, FACE_UP, FACE_LEFT)
			|| and3Sides(FACE_DOWN, FACE_BACK, FACE_RIGHT);
		}

		function shouldRotateCounterClockwise90() {
			return and3Sides(FACE_UP, FACE_BACK, FACE_LEFT)
			|| and3Sides(FACE_DOWN, FACE_FRONT, FACE_RIGHT)
			|| and3Sides(FACE_RIGHT, FACE_UP, FACE_FRONT)
			|| and3Sides(FACE_LEFT, FACE_UP, FACE_BACK)
			|| and3Sides(FACE_FRONT, FACE_DOWN, FACE_RIGHT)
			|| and3Sides(FACE_DOWN, FACE_FRONT, FACE_LEFT);
		}

		function shouldRotate180() {
			return and3Sides([FACE_UP, FACE_DOWN], FACE_RIGHT, [FACE_FRONT, FACE_BACK])
			|| and3Sides([FACE_LEFT, FACE_RIGHT], [FACE_FRONT, FACE_BACK], FACE_UP)
			|| and3Sides(FACE_FRONT, FACE_RIGHT, FACE_UP);
		}

		function shouldRotate180AndReflectByX() {
			return and3Sides(FACE_BACK, FACE_RIGHT, FACE_DOWN);
		}

		function shouldRotate180AndReflectByY() {
			return and3Sides(FACE_BACK, FACE_LEFT, FACE_UP);
		}

		function shouldRotateCounterClockwise90AndReflectByY() {
			return and3Sides(FACE_BACK, FACE_UP, FACE_RIGHT);
		}

		function shouldRotateClockwise90AndReflectByY() {
			return and3Sides(FACE_BACK, FACE_DOWN, FACE_LEFT);
		}

		function and3Sides(front, left, down) {
			var result = true;
			var tests = [front, left, down];
			var sources = [FACE_FRONT, FACE_LEFT, FACE_DOWN];
			for (var i = sources.length - 1; i >= 0; i--) {
				var test = tests[i];
				if (typeof test === 'Array') {
					result = result && test.indexOf(_this.faces[sources[i]].name) !== ItemNotFound;
				} else {
					result = result && test === _this.faces[sources[i]].name;
				}
			};

			return result;
		}
	}

	return {
		Cube: Cube,
		CubeFace: {
			Front: FACE_FRONT,
			Back: FACE_BACK,
			Up: FACE_UP,
			Down: FACE_DOWN,
			Right: FACE_RIGHT,
			Left: FACE_LEFT
		}
	};
})();