var RubikCubeLib = (function() {
	function Cube() {
	    this.fillSides();
	}

	Cube.prototype.fillSides = function() {     
	    this.frontSide = this.createSide("red");
	    this.backSide = this.createSide("orange");
	    this.rightSide = this.createSide("blue");
	    this.leftSide = this.createSide("green");
	    this.topSide = this.createSide("yellow");
	    this.bottomSide = this.createSide("white");

	    var _this = this;

	    this.allSides = function() {
	    	return [_this.frontSide, _this.backSide, _this.rightSide, _this.leftSide, _this.topSide, _this.bottomSide];
	    }
	};

	Cube.prototype.createSide = function(color) {  
	    var side = [];
	    for (var i = 0; i < 9; i++) {
	        side.push(color);
	    }
	    return side;
	};

	Cube.prototype.transformCounterClockwise = function (array) {
	    var newtransformArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
	    for (var i = 0; i < 9; i++) {
	        newtransformArray[(3 * i + 2) % 10] = array[i];
	    }
	    return newtransformArray;
	}

	Cube.prototype.transformClockwise = function (array) {  
	    var newtransformArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
	    for (var i = 0; i < 9; i++) {
	        newtransformArray[i] = array[(3 * i + 2) % 10];
	    }
	    return newtransformArray;
	}

	Cube.prototype.generateArrayCopy = function(array) { 
	    var copy = [];
	    for (var i = 0; i < 9; i++) {
	        copy[i] = array[i];
	    }
	    return copy;
	}

	Cube.prototype.toFrontCoordinate = function(value) { 
	    return value;
	}

	Cube.prototype.toInvertCoordinate = function (value) { 
	    return 8 - value;
	}


	Cube.prototype.rotateTop = function (columnNumber) {
	    var i;
	    if (columnNumber != null) {
	        // Left Row rotation
	        var thisSide = this.generateArrayCopy(this.frontSide);

	        // top to front
	        for (i = 0; i < 3; i++) {
	            this.frontSide[this.toFrontCoordinate(i * 3 + columnNumber)] = this.topSide[this.toFrontCoordinate(i * 3 + columnNumber)];
	        }

	        // Back to top
	        for (i = 0; i < 3; i++) {
	            this.topSide[this.toFrontCoordinate(i * 3 + columnNumber)] = this.backSide[this.toInvertCoordinate(i * 3 + columnNumber)];
	        }

	        //Bottom to back
	        for (i = 0; i < 3; i++) {
	            this.backSide[this.toInvertCoordinate(i * 3 + columnNumber)] = this.bottomSide[this.toFrontCoordinate(i * 3 + columnNumber)];
	        }

	        //Front to bottom
	        for (i = 0; i < 3; i++) {
	            this.bottomSide[this.toFrontCoordinate(i * 3 + columnNumber)] = thisSide[this.toFrontCoordinate(i * 3 + columnNumber)];
	        }

	        // Transporent left or rigth side
	        if (columnNumber == 0) {
	            this.leftSide = this.transformCounterClockwise(this.leftSide);
	        }
	        if (columnNumber == 2) {
	            this.rightSide = this.transformClockwise(this.rightSide);
	        }
	    }
	    else {
	        for (i = 0; i < 3; i++) {
	            this.rotateTop(i);
	        }
	    }
	};

	Cube.prototype.rotateBottom = function(columnNumber) {
	    if (columnNumber != null) {
	        // Left Row rotation
	        var thisSide = this.generateArrayCopy(this.frontSide);

	        // bottom to front
	        var i;

	        for (i = 0; i < 3; i++) {
	            this.frontSide[this.toFrontCoordinate(i * 3 + columnNumber)] = this.bottomSide[this.toFrontCoordinate(i * 3 + columnNumber)];
	        }

	        // Back to bottom
	        for (i = 0; i < 3; i++) {
	            this.bottomSide[this.toFrontCoordinate(i * 3 + columnNumber)] = this.backSide[this.toInvertCoordinate(i * 3 + columnNumber)];
	        }

	        // top to back
	        for (i = 0; i < 3; i++) {
	            this.backSide[this.toInvertCoordinate(i * 3 + columnNumber)] = this.topSide[this.toFrontCoordinate(i * 3 + columnNumber)];
	        }

	        // Front to top
	        for (i = 0; i < 3; i++) {
	            this.topSide[this.toFrontCoordinate(i * 3 + columnNumber)] = thisSide[this.toFrontCoordinate(i * 3 + columnNumber)];
	        }

	        // Transporent left or rigth side
	        if (columnNumber == 0) {
	            this.leftSide = this.transformClockwise(this.leftSide);
	        }
	        if (columnNumber == 2) {
	            this.rightSide = this.transformCounterClockwise(this.rightSide);
	        }
	    }
	    else {
	        for (i = 0; i < 3; i++) {
	            this.rotateBottom(i);
	        }
	    }
	};

	Cube.prototype.rotateRight = function(rowNumber) {
	    var i;
	    if (rowNumber != null) {
	        // Left Row rotation
	        var thisSide = this.generateArrayCopy(this.frontSide);

	        // left to front
	        for (i = 0; i < 3; i++) {
	            this.frontSide[this.toFrontCoordinate(i + rowNumber * 3)] = this.leftSide[this.toFrontCoordinate(i + rowNumber * 3)];
	        }

	        // Back to left
	        for (i = 0; i < 3; i++) {
	            this.leftSide[this.toFrontCoordinate(i + rowNumber * 3)] = this.backSide[this.toFrontCoordinate(i + rowNumber * 3)];
	        }

	        // right to back
	        for (i = 0; i < 3; i++) {
	            this.backSide[this.toFrontCoordinate(i + rowNumber * 3)] = this.rightSide[this.toFrontCoordinate(i + rowNumber * 3)];
	        }

	        // Front to right
	        for (i = 0; i < 3; i++) {
	            this.rightSide[this.toFrontCoordinate(i + rowNumber * 3)] = thisSide[this.toFrontCoordinate(i + rowNumber * 3)];
	        }

	        // Transporent top or bottom side
	        if (rowNumber == 0) {
	            this.topSide = this.transformCounterClockwise(this.topSide);
	        }
	        if (rowNumber == 2) {
	            this.bottomSide = this.transformClockwise(this.bottomSide);
	        }
	    }
	    else {
	        for (i = 0; i < 3; i++) {
	            this.rotateRight(i);
	        }
	    }
	};

	Cube.prototype.rotateLeft = function(rowNumber) {
	    var i;
	    if (rowNumber != null) {
	        // Left Row rotation
	        var thisSide = this.generateArrayCopy(this.frontSide);

	        // right to front
	        for (i = 0; i < 3; i++) {
	            this.frontSide[this.toFrontCoordinate(i + rowNumber * 3)] = this.rightSide[this.toFrontCoordinate(i + rowNumber * 3)];
	        }

	        // Back to right
	        for (i = 0; i < 3; i++) {
	            this.rightSide[this.toFrontCoordinate(i + rowNumber * 3)] = this.backSide[this.toFrontCoordinate(i + rowNumber * 3)];
	        }

	        // left to back
	        for (i = 0; i < 3; i++) {
	            this.backSide[this.toFrontCoordinate(i + rowNumber * 3)] = this.leftSide[this.toFrontCoordinate(i + rowNumber * 3)];
	        }

	        // Front to left
	        for (i = 0; i < 3; i++) {
	            this.leftSide[this.toFrontCoordinate(i + rowNumber * 3)] = thisSide[this.toFrontCoordinate(i + rowNumber * 3)];
	        }

	        // Transporent top or bottom side
	        if (rowNumber == 0) {
	            this.topSide = this.transformCounterClockwise(this.topSide);
	        }
	        if (rowNumber == 2) {
	            this.bottomSide = this.transformClockwise(this.bottomSide);
	        }
	    }
	    else {
	        for (i = 0; i < 3; i++) {
	            this.rotateLeft(i);
	        }
	    }
	};

	Cube.prototype.isSolved = function() {
		var allSides = this.allSides();
		for (var i = allSides.length - 1; i >= 0; i--) {
			for (var j = 8;j >= 0; j--) {
				if (allSides[i][j] !== allSides[i][(j === 8 )? 0 : (j + 1)]) {
					return false;
				}
			};
		};

		return true;
	};

	return {
		Cube: Cube
	};
})();