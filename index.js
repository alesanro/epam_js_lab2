var MyUI = (function(window, Rubik) {
	var debugMode = true;

	var cube = new Rubik.Cube();

	updateUI();
	
	return {
		shuffleCube: shuffle,
		cubeRight: rotateCubeRight,
		cubeLeft: rotateCubeLeft,
		cubeUp: rotateCubeUp,
		cubeDown: rotateCubeDown,
		rowLeft: rotateRowLeft,
		rowRight: rotateRowRight,
		columnUp: rotateColumnUp,
		columnDown: rotateColumnDown
	};

	function debug(cb) {
		if (debugMode && cb) {
			cb();
		}
	}

	function carry(f) {
		if (typeof f !== 'function') {
			throw 'First argument must be a function';
		}

		var args = arguments, curryArgs = [];
		var length = f.length;

		for (var i = 1; i < args.length; i++) {
			curryArgs[i - 1] = args[i];
		}

		return function () {
			var argsArr = Array.prototype.slice.call(arguments);    

			curryArgs = curryArgs.concat(argsArr);
			return f.apply(this, curryArgs);
		}
	}
	
	function shuffle() {
		var localRotations = [carry(_rotateRowLeft, 1), carry(_rotateRowLeft, 2), carry(_rotateRowLeft, 3), 
								carry(_rotateRowRight, 1), carry(_rotateRowRight, 2), carry(_rotateRowRight, 3),
								carry(_rotateColumnUp, 1), carry(_rotateColumnUp, 2), carry(_rotateColumnUp, 3), 
								carry(_rotateColumnDown, 1), carry(_rotateColumnDown, 2), carry(_rotateColumnDown, 3)];
		var cubeRotations = [_rotateCubeUp, _rotateCubeDown, _rotateCubeLeft, _rotateCubeRight];
		for(var idx = 0, repeat = 1000, rotateCubeEvery = 5; idx < repeat; ++idx) {
			if (idx % rotateCubeEvery == 0) {
				cubeRotations[getRandomInt(0, cubeRotations.length)]();
			} else {
				localRotations[getRandomInt(0, localRotations.length)]();
			}
		}

		updateUI();

		function getRandomInt(min, max) {
			return Math.floor(Math.random() * (max - min)) + min;
		}
	}

	function rotateCubeUp() {
		_rotateCubeUp();
		updateUI();
	}

	function _rotateCubeUp() {
		cube.rotateTop();
	}

	function rotateCubeRight() {
		_rotateCubeRight();
		updateUI();
	}

	function _rotateCubeRight() {
		cube.rotateRight();
	}

	function rotateCubeLeft() {
		_rotateCubeLeft();
		updateUI();
	}

	function _rotateCubeLeft() {
		cube.rotateLeft();
	}

	function rotateCubeDown() {
		_rotateCubeDown();
		updateUI();
	}

	function _rotateCubeDown() {
		cube.rotateBottom();
	}

	function rotateRowLeft(row) {
		debug(function() {
			console.log("row " + row + " left...");
		});

		_rotateRowLeft(row);
		updateUI();
	}

	function _rotateRowLeft(row) {
		if (typeof row === 'undefined') {
			cube.rotateLeft();
		} else if (row === 1) {
			cube.rotateLeft(0);
		} else if (row === 2) {
			cube.rotateLeft(1);
		} else if (row === 3) {
			cube.rotateLeft(2);
		} else {
			console.log("Need valid row number for rotation");
		}
	}

	function rotateRowRight(row) {
		debug(function() {
			console.log("row " + row + " right...");
		});

		_rotateRowRight(row);
		updateUI();
	}

	function _rotateRowRight(row) {
		if (typeof row === 'undefined') {
			cube.rotateRight();
		} else if (row === 1) {
			cube.rotateRight(0);
		} else if (row === 2) {
			cube.rotateRight(1);
		} else if (row === 3) {
			cube.rotateRight(2);
		} else {
			console.log("Need valid row number for rotation");
		}
	}

	function rotateColumnUp(column) {
		debug(function() {
			console.log("column " + column + " up...");
		});

		_rotateColumnUp(column);
		updateUI();
	}

	function _rotateColumnUp(column) {
		if (typeof column === 'undefined') {
			cube.rotateTop();
		} else if (column === 1) {
			cube.rotateTop(0);
		} else if (column === 2) {
			cube.rotateTop(1);
		} else if (column === 3) {
			cube.rotateTop(2);
		} else {
			console.log("Need valid column number for rotation");
		}
	}

	function rotateColumnDown(column) {
		debug(function() {
			console.log("column " + column + " down...");
		})
		_rotateColumnDown(column);
		updateUI();
	}

	function _rotateColumnDown(column) {
		if (typeof column === 'undefined') {
			cube.rotateBottom();
		} else if (column === 1) {
			cube.rotateBottom(0);
		} else if (column === 2) {
			cube.rotateBottom(1);
		} else if (column === 3) {
			cube.rotateBottom(2);
		} else {
			console.log("Need valid column number for rotation...");
		}
	}

	function updateUI() {
		var document = window.document;

		for (var i = 0; i < 3; i++) {
			for (var j = 0; j < 3; j++) {
				document.getElementById("i" + (i+1) + "" + (j+1)).style.backgroundColor = cube.frontSide[i * 3 + j];
			}
		}

		var userOutput = document.getElementById('output');
		if (cube.isSolved()) {
			userOutput.innerHTML = "You do it! Congratulations!!!";
			userOutput.style.color = 'green';
		} else {
			userOutput.innerHTML = "";
		}
	}

})(window, RubikCubeLib);